# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/workspace/WasmEdge/_deps/boost-src"
  "/workspace/WasmEdge/_deps/boost-build"
  "/workspace/WasmEdge/_deps/boost-subbuild/boost-populate-prefix"
  "/workspace/WasmEdge/_deps/boost-subbuild/boost-populate-prefix/tmp"
  "/workspace/WasmEdge/_deps/boost-subbuild/boost-populate-prefix/src/boost-populate-stamp"
  "/workspace/WasmEdge/_deps/boost-subbuild/boost-populate-prefix/src"
  "/workspace/WasmEdge/_deps/boost-subbuild/boost-populate-prefix/src/boost-populate-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/workspace/WasmEdge/_deps/boost-subbuild/boost-populate-prefix/src/boost-populate-stamp/${subDir}")
endforeach()
