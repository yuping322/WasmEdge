# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/workspace/WasmEdge/_deps/rapidjson-src"
  "/workspace/WasmEdge/_deps/rapidjson-build"
  "/workspace/WasmEdge/_deps/rapidjson-subbuild/rapidjson-populate-prefix"
  "/workspace/WasmEdge/_deps/rapidjson-subbuild/rapidjson-populate-prefix/tmp"
  "/workspace/WasmEdge/_deps/rapidjson-subbuild/rapidjson-populate-prefix/src/rapidjson-populate-stamp"
  "/workspace/WasmEdge/_deps/rapidjson-subbuild/rapidjson-populate-prefix/src"
  "/workspace/WasmEdge/_deps/rapidjson-subbuild/rapidjson-populate-prefix/src/rapidjson-populate-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/workspace/WasmEdge/_deps/rapidjson-subbuild/rapidjson-populate-prefix/src/rapidjson-populate-stamp/${subDir}")
endforeach()
