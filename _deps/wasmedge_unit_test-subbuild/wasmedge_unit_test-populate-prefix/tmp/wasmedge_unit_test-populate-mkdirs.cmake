# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/workspace/WasmEdge/_deps/wasmedge_unit_test-src"
  "/workspace/WasmEdge/_deps/wasmedge_unit_test-build"
  "/workspace/WasmEdge/_deps/wasmedge_unit_test-subbuild/wasmedge_unit_test-populate-prefix"
  "/workspace/WasmEdge/_deps/wasmedge_unit_test-subbuild/wasmedge_unit_test-populate-prefix/tmp"
  "/workspace/WasmEdge/_deps/wasmedge_unit_test-subbuild/wasmedge_unit_test-populate-prefix/src/wasmedge_unit_test-populate-stamp"
  "/workspace/WasmEdge/_deps/wasmedge_unit_test-subbuild/wasmedge_unit_test-populate-prefix/src"
  "/workspace/WasmEdge/_deps/wasmedge_unit_test-subbuild/wasmedge_unit_test-populate-prefix/src/wasmedge_unit_test-populate-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/workspace/WasmEdge/_deps/wasmedge_unit_test-subbuild/wasmedge_unit_test-populate-prefix/src/wasmedge_unit_test-populate-stamp/${subDir}")
endforeach()
