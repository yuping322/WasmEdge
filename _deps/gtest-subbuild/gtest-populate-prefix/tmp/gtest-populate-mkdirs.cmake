# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/workspace/WasmEdge/_deps/gtest-src"
  "/workspace/WasmEdge/_deps/gtest-build"
  "/workspace/WasmEdge/_deps/gtest-subbuild/gtest-populate-prefix"
  "/workspace/WasmEdge/_deps/gtest-subbuild/gtest-populate-prefix/tmp"
  "/workspace/WasmEdge/_deps/gtest-subbuild/gtest-populate-prefix/src/gtest-populate-stamp"
  "/workspace/WasmEdge/_deps/gtest-subbuild/gtest-populate-prefix/src"
  "/workspace/WasmEdge/_deps/gtest-subbuild/gtest-populate-prefix/src/gtest-populate-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/workspace/WasmEdge/_deps/gtest-subbuild/gtest-populate-prefix/src/gtest-populate-stamp/${subDir}")
endforeach()
