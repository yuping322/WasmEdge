
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/thirdparty/blake3/blake3.c" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3.c.o" "gcc" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3.c.o.d"
  "/workspace/WasmEdge/thirdparty/blake3/blake3_avx2.c" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_avx2.c.o" "gcc" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_avx2.c.o.d"
  "/workspace/WasmEdge/thirdparty/blake3/blake3_avx512.c" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_avx512.c.o" "gcc" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_avx512.c.o.d"
  "/workspace/WasmEdge/thirdparty/blake3/blake3_dispatch.c" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_dispatch.c.o" "gcc" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_dispatch.c.o.d"
  "/workspace/WasmEdge/thirdparty/blake3/blake3_portable.c" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_portable.c.o" "gcc" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_portable.c.o.d"
  "/workspace/WasmEdge/thirdparty/blake3/blake3_sse2.c" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_sse2.c.o" "gcc" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_sse2.c.o.d"
  "/workspace/WasmEdge/thirdparty/blake3/blake3_sse41.c" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_sse41.c.o" "gcc" "thirdparty/blake3/CMakeFiles/utilBlake3.dir/blake3_sse41.c.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
