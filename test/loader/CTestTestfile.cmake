# CMake generated Testfile for 
# Source directory: /workspace/WasmEdge/test/loader
# Build directory: /workspace/WasmEdge/test/loader
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(wasmedgeLoaderFileMgrTests "wasmedgeLoaderFileMgrTests")
set_tests_properties(wasmedgeLoaderFileMgrTests PROPERTIES  _BACKTRACE_TRIPLES "/workspace/WasmEdge/test/loader/CMakeLists.txt;8;add_test;/workspace/WasmEdge/test/loader/CMakeLists.txt;0;")
add_test(wasmedgeLoaderASTTests "wasmedgeLoaderASTTests")
set_tests_properties(wasmedgeLoaderASTTests PROPERTIES  _BACKTRACE_TRIPLES "/workspace/WasmEdge/test/loader/CMakeLists.txt;32;add_test;/workspace/WasmEdge/test/loader/CMakeLists.txt;0;")
