
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/test/loader/descriptionTest.cpp" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/descriptionTest.cpp.o" "gcc" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/descriptionTest.cpp.o.d"
  "/workspace/WasmEdge/test/loader/expressionTest.cpp" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/expressionTest.cpp.o" "gcc" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/expressionTest.cpp.o.d"
  "/workspace/WasmEdge/test/loader/instructionTest.cpp" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/instructionTest.cpp.o" "gcc" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/instructionTest.cpp.o.d"
  "/workspace/WasmEdge/test/loader/moduleTest.cpp" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/moduleTest.cpp.o" "gcc" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/moduleTest.cpp.o.d"
  "/workspace/WasmEdge/test/loader/sectionTest.cpp" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/sectionTest.cpp.o" "gcc" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/sectionTest.cpp.o.d"
  "/workspace/WasmEdge/test/loader/segmentTest.cpp" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/segmentTest.cpp.o" "gcc" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/segmentTest.cpp.o.d"
  "/workspace/WasmEdge/test/loader/typeTest.cpp" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/typeTest.cpp.o" "gcc" "test/loader/CMakeFiles/wasmedgeLoaderASTTests.dir/typeTest.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/loader/CMakeFiles/wasmedgeLoader.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/loader/CMakeFiles/wasmedgeLoaderFileMgr.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/system/CMakeFiles/wasmedgeSystem.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
