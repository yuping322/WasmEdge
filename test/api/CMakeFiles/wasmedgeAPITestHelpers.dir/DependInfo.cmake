
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/test/api/hostfunc_c.c" "test/api/CMakeFiles/wasmedgeAPITestHelpers.dir/hostfunc_c.c.o" "gcc" "test/api/CMakeFiles/wasmedgeAPITestHelpers.dir/hostfunc_c.c.o.d"
  "/workspace/WasmEdge/test/api/helper.cpp" "test/api/CMakeFiles/wasmedgeAPITestHelpers.dir/helper.cpp.o" "gcc" "test/api/CMakeFiles/wasmedgeAPITestHelpers.dir/helper.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/lib/api/CMakeFiles/wasmedge_c_shared.dir/DependInfo.cmake"
  "/workspace/WasmEdge/test/spec/CMakeFiles/wasmedgeTestSpec.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
