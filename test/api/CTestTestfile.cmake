# CMake generated Testfile for 
# Source directory: /workspace/WasmEdge/test/api
# Build directory: /workspace/WasmEdge/test/api
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(wasmedgeAPIUnitTests "wasmedgeAPIUnitTests")
set_tests_properties(wasmedgeAPIUnitTests PROPERTIES  _BACKTRACE_TRIPLES "/workspace/WasmEdge/test/api/CMakeLists.txt;29;add_test;/workspace/WasmEdge/test/api/CMakeLists.txt;0;")
add_test(wasmedgeAPIVMCoreTests "wasmedgeAPIVMCoreTests")
set_tests_properties(wasmedgeAPIVMCoreTests PROPERTIES  _BACKTRACE_TRIPLES "/workspace/WasmEdge/test/api/CMakeLists.txt;43;add_test;/workspace/WasmEdge/test/api/CMakeLists.txt;0;")
add_test(wasmedgeAPIStepsCoreTests "wasmedgeAPIStepsCoreTests")
set_tests_properties(wasmedgeAPIStepsCoreTests PROPERTIES  _BACKTRACE_TRIPLES "/workspace/WasmEdge/test/api/CMakeLists.txt;58;add_test;/workspace/WasmEdge/test/api/CMakeLists.txt;0;")
add_test(wasmedgeAPIAOTCoreTests "wasmedgeAPIAOTCoreTests")
set_tests_properties(wasmedgeAPIAOTCoreTests PROPERTIES  _BACKTRACE_TRIPLES "/workspace/WasmEdge/test/api/CMakeLists.txt;74;add_test;/workspace/WasmEdge/test/api/CMakeLists.txt;0;")
