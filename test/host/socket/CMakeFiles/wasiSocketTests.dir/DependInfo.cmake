
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/test/host/socket/wasi_socket.cpp" "test/host/socket/CMakeFiles/wasiSocketTests.dir/wasi_socket.cpp.o" "gcc" "test/host/socket/CMakeFiles/wasiSocketTests.dir/wasi_socket.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/system/CMakeFiles/wasmedgeSystem.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
