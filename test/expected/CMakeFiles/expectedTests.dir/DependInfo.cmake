
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/test/expected/assignment.cpp" "test/expected/CMakeFiles/expectedTests.dir/assignment.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/assignment.cpp.o.d"
  "/workspace/WasmEdge/test/expected/bases.cpp" "test/expected/CMakeFiles/expectedTests.dir/bases.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/bases.cpp.o.d"
  "/workspace/WasmEdge/test/expected/constexpr.cpp" "test/expected/CMakeFiles/expectedTests.dir/constexpr.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/constexpr.cpp.o.d"
  "/workspace/WasmEdge/test/expected/constructors.cpp" "test/expected/CMakeFiles/expectedTests.dir/constructors.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/constructors.cpp.o.d"
  "/workspace/WasmEdge/test/expected/emplace.cpp" "test/expected/CMakeFiles/expectedTests.dir/emplace.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/emplace.cpp.o.d"
  "/workspace/WasmEdge/test/expected/extensions.cpp" "test/expected/CMakeFiles/expectedTests.dir/extensions.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/extensions.cpp.o.d"
  "/workspace/WasmEdge/test/expected/gtest.cpp" "test/expected/CMakeFiles/expectedTests.dir/gtest.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/gtest.cpp.o.d"
  "/workspace/WasmEdge/test/expected/issues.cpp" "test/expected/CMakeFiles/expectedTests.dir/issues.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/issues.cpp.o.d"
  "/workspace/WasmEdge/test/expected/noexcept.cpp" "test/expected/CMakeFiles/expectedTests.dir/noexcept.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/noexcept.cpp.o.d"
  "/workspace/WasmEdge/test/expected/observers.cpp" "test/expected/CMakeFiles/expectedTests.dir/observers.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/observers.cpp.o.d"
  "/workspace/WasmEdge/test/expected/relops.cpp" "test/expected/CMakeFiles/expectedTests.dir/relops.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/relops.cpp.o.d"
  "/workspace/WasmEdge/test/expected/swap.cpp" "test/expected/CMakeFiles/expectedTests.dir/swap.cpp.o" "gcc" "test/expected/CMakeFiles/expectedTests.dir/swap.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
