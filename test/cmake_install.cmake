# Install script for directory: /workspace/WasmEdge/test

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/workspace/WasmEdge/_deps/gtest-build/cmake_install.cmake")
  include("/workspace/WasmEdge/test/aot/cmake_install.cmake")
  include("/workspace/WasmEdge/test/mixcall/cmake_install.cmake")
  include("/workspace/WasmEdge/test/spec/cmake_install.cmake")
  include("/workspace/WasmEdge/test/loader/cmake_install.cmake")
  include("/workspace/WasmEdge/test/executor/cmake_install.cmake")
  include("/workspace/WasmEdge/test/thread/cmake_install.cmake")
  include("/workspace/WasmEdge/test/api/cmake_install.cmake")
  include("/workspace/WasmEdge/test/externref/cmake_install.cmake")
  include("/workspace/WasmEdge/test/host/wasmedge_process/cmake_install.cmake")
  include("/workspace/WasmEdge/test/host/socket/cmake_install.cmake")
  include("/workspace/WasmEdge/test/host/wasi/cmake_install.cmake")
  include("/workspace/WasmEdge/test/expected/cmake_install.cmake")
  include("/workspace/WasmEdge/test/span/cmake_install.cmake")
  include("/workspace/WasmEdge/test/po/cmake_install.cmake")
  include("/workspace/WasmEdge/test/memlimit/cmake_install.cmake")
  include("/workspace/WasmEdge/test/errinfo/cmake_install.cmake")

endif()

