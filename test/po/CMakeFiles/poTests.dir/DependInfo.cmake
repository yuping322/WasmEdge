
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/test/po/gtest.cpp" "test/po/CMakeFiles/poTests.dir/gtest.cpp.o" "gcc" "test/po/CMakeFiles/poTests.dir/gtest.cpp.o.d"
  "/workspace/WasmEdge/test/po/help.cpp" "test/po/CMakeFiles/poTests.dir/help.cpp.o" "gcc" "test/po/CMakeFiles/poTests.dir/help.cpp.o.d"
  "/workspace/WasmEdge/test/po/po.cpp" "test/po/CMakeFiles/poTests.dir/po.cpp.o" "gcc" "test/po/CMakeFiles/poTests.dir/po.cpp.o.d"
  "/workspace/WasmEdge/test/po/subcommand.cpp" "test/po/CMakeFiles/poTests.dir/subcommand.cpp.o" "gcc" "test/po/CMakeFiles/poTests.dir/subcommand.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/lib/po/CMakeFiles/wasmedgePO.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/gtest-build/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
