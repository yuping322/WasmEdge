# CMake generated Testfile for 
# Source directory: /workspace/WasmEdge/test
# Build directory: /workspace/WasmEdge/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("../_deps/gtest-build")
subdirs("aot")
subdirs("mixcall")
subdirs("spec")
subdirs("loader")
subdirs("executor")
subdirs("thread")
subdirs("api")
subdirs("externref")
subdirs("host/wasmedge_process")
subdirs("host/socket")
subdirs("host/wasi")
subdirs("expected")
subdirs("span")
subdirs("po")
subdirs("memlimit")
subdirs("errinfo")
