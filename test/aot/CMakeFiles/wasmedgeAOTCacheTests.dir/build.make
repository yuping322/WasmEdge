# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.23

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /home/linuxbrew/.linuxbrew/Cellar/cmake/3.23.1_1/bin/cmake

# The command to remove a file.
RM = /home/linuxbrew/.linuxbrew/Cellar/cmake/3.23.1_1/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /workspace/WasmEdge

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /workspace/WasmEdge

# Include any dependencies generated for this target.
include test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/compiler_depend.make

# Include the progress variables for this target.
include test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/progress.make

# Include the compile flags for this target's objects.
include test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/flags.make

test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.o: test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/flags.make
test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.o: test/aot/AOTCacheTest.cpp
test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.o: test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/workspace/WasmEdge/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.o"
	cd /workspace/WasmEdge/test/aot && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.o -MF CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.o.d -o CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.o -c /workspace/WasmEdge/test/aot/AOTCacheTest.cpp

test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.i"
	cd /workspace/WasmEdge/test/aot && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /workspace/WasmEdge/test/aot/AOTCacheTest.cpp > CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.i

test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.s"
	cd /workspace/WasmEdge/test/aot && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /workspace/WasmEdge/test/aot/AOTCacheTest.cpp -o CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.s

# Object files for target wasmedgeAOTCacheTests
wasmedgeAOTCacheTests_OBJECTS = \
"CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.o"

# External object files for target wasmedgeAOTCacheTests
wasmedgeAOTCacheTests_EXTERNAL_OBJECTS =

test/aot/wasmedgeAOTCacheTests: test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/AOTCacheTest.cpp.o
test/aot/wasmedgeAOTCacheTests: test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/build.make
test/aot/wasmedgeAOTCacheTests: lib/libgtest.a
test/aot/wasmedgeAOTCacheTests: lib/libgtest_main.a
test/aot/wasmedgeAOTCacheTests: lib/aot/libwasmedgeAOT.a
test/aot/wasmedgeAOTCacheTests: lib/libgtest.a
test/aot/wasmedgeAOTCacheTests: lib/system/libwasmedgeSystem.a
test/aot/wasmedgeAOTCacheTests: lib/common/libwasmedgeCommon.a
test/aot/wasmedgeAOTCacheTests: _deps/spdlog-build/libspdlog.a
test/aot/wasmedgeAOTCacheTests: thirdparty/blake3/libutilBlake3.a
test/aot/wasmedgeAOTCacheTests: /usr/lib/llvm-12/lib/liblldELF.a
test/aot/wasmedgeAOTCacheTests: /usr/lib/llvm-12/lib/liblldDriver.a
test/aot/wasmedgeAOTCacheTests: /usr/lib/llvm-12/lib/liblldMachO.a
test/aot/wasmedgeAOTCacheTests: /usr/lib/llvm-12/lib/liblldCommon.a
test/aot/wasmedgeAOTCacheTests: /usr/lib/llvm-12/lib/liblldReaderWriter.a
test/aot/wasmedgeAOTCacheTests: /usr/lib/llvm-12/lib/liblldYAML.a
test/aot/wasmedgeAOTCacheTests: /usr/lib/llvm-12/lib/liblldCore.a
test/aot/wasmedgeAOTCacheTests: /usr/lib/llvm-12/lib/libLLVM-12.so.1
test/aot/wasmedgeAOTCacheTests: test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/workspace/WasmEdge/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable wasmedgeAOTCacheTests"
	cd /workspace/WasmEdge/test/aot && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/wasmedgeAOTCacheTests.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/build: test/aot/wasmedgeAOTCacheTests
.PHONY : test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/build

test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/clean:
	cd /workspace/WasmEdge/test/aot && $(CMAKE_COMMAND) -P CMakeFiles/wasmedgeAOTCacheTests.dir/cmake_clean.cmake
.PHONY : test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/clean

test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/depend:
	cd /workspace/WasmEdge && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /workspace/WasmEdge /workspace/WasmEdge/test/aot /workspace/WasmEdge /workspace/WasmEdge/test/aot /workspace/WasmEdge/test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : test/aot/CMakeFiles/wasmedgeAOTCacheTests.dir/depend

