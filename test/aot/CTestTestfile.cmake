# CMake generated Testfile for 
# Source directory: /workspace/WasmEdge/test/aot
# Build directory: /workspace/WasmEdge/test/aot
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(wasmedgeAOTCoreTests "wasmedgeAOTCoreTests")
set_tests_properties(wasmedgeAOTCoreTests PROPERTIES  _BACKTRACE_TRIPLES "/workspace/WasmEdge/test/aot/CMakeLists.txt;8;add_test;/workspace/WasmEdge/test/aot/CMakeLists.txt;0;")
add_test(wasmedgeAOTCacheTests "wasmedgeAOTCacheTests")
set_tests_properties(wasmedgeAOTCacheTests PROPERTIES  _BACKTRACE_TRIPLES "/workspace/WasmEdge/test/aot/CMakeLists.txt;24;add_test;/workspace/WasmEdge/test/aot/CMakeLists.txt;0;")
add_test(wasmedgeAOTBlake3Tests "wasmedgeAOTBlake3Tests")
set_tests_properties(wasmedgeAOTBlake3Tests PROPERTIES  _BACKTRACE_TRIPLES "/workspace/WasmEdge/test/aot/CMakeLists.txt;36;add_test;/workspace/WasmEdge/test/aot/CMakeLists.txt;0;")
