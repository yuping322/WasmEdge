# CMake generated Testfile for 
# Source directory: /workspace/WasmEdge
# Build directory: /workspace/WasmEdge
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("test")
subdirs("include")
subdirs("lib")
subdirs("plugins")
subdirs("thirdparty")
subdirs("tools")
subdirs("rpm")
