
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/lib/aot/blake3.cpp" "lib/aot/CMakeFiles/wasmedgeAOT.dir/blake3.cpp.o" "gcc" "lib/aot/CMakeFiles/wasmedgeAOT.dir/blake3.cpp.o.d"
  "/workspace/WasmEdge/lib/aot/cache.cpp" "lib/aot/CMakeFiles/wasmedgeAOT.dir/cache.cpp.o" "gcc" "lib/aot/CMakeFiles/wasmedgeAOT.dir/cache.cpp.o.d"
  "/workspace/WasmEdge/lib/aot/compiler.cpp" "lib/aot/CMakeFiles/wasmedgeAOT.dir/compiler.cpp.o" "gcc" "lib/aot/CMakeFiles/wasmedgeAOT.dir/compiler.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/system/CMakeFiles/wasmedgeSystem.dir/DependInfo.cmake"
  "/workspace/WasmEdge/thirdparty/blake3/CMakeFiles/utilBlake3.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
