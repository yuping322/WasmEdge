
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/lib/loader/ast/description.cpp" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/description.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/description.cpp.o.d"
  "/workspace/WasmEdge/lib/loader/ast/expression.cpp" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/expression.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/expression.cpp.o.d"
  "/workspace/WasmEdge/lib/loader/ast/instruction.cpp" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/instruction.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/instruction.cpp.o.d"
  "/workspace/WasmEdge/lib/loader/ast/module.cpp" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/module.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/module.cpp.o.d"
  "/workspace/WasmEdge/lib/loader/ast/section.cpp" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/section.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/section.cpp.o.d"
  "/workspace/WasmEdge/lib/loader/ast/segment.cpp" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/segment.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/segment.cpp.o.d"
  "/workspace/WasmEdge/lib/loader/ast/type.cpp" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/type.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoader.dir/ast/type.cpp.o.d"
  "/workspace/WasmEdge/lib/loader/loader.cpp" "lib/loader/CMakeFiles/wasmedgeLoader.dir/loader.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoader.dir/loader.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/loader/CMakeFiles/wasmedgeLoaderFileMgr.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/system/CMakeFiles/wasmedgeSystem.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
