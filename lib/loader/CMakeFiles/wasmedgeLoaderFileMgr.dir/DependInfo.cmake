
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/lib/loader/filemgr.cpp" "lib/loader/CMakeFiles/wasmedgeLoaderFileMgr.dir/filemgr.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoaderFileMgr.dir/filemgr.cpp.o.d"
  "/workspace/WasmEdge/lib/loader/ldmgr.cpp" "lib/loader/CMakeFiles/wasmedgeLoaderFileMgr.dir/ldmgr.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoaderFileMgr.dir/ldmgr.cpp.o.d"
  "/workspace/WasmEdge/lib/loader/shared_library.cpp" "lib/loader/CMakeFiles/wasmedgeLoaderFileMgr.dir/shared_library.cpp.o" "gcc" "lib/loader/CMakeFiles/wasmedgeLoaderFileMgr.dir/shared_library.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/system/CMakeFiles/wasmedgeSystem.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
