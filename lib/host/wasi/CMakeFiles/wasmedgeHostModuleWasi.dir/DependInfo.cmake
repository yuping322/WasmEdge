
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/lib/host/wasi/clock-linux.cpp" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/clock-linux.cpp.o" "gcc" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/clock-linux.cpp.o.d"
  "/workspace/WasmEdge/lib/host/wasi/environ-linux.cpp" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/environ-linux.cpp.o" "gcc" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/environ-linux.cpp.o.d"
  "/workspace/WasmEdge/lib/host/wasi/environ.cpp" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/environ.cpp.o" "gcc" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/environ.cpp.o.d"
  "/workspace/WasmEdge/lib/host/wasi/inode-linux.cpp" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/inode-linux.cpp.o" "gcc" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/inode-linux.cpp.o.d"
  "/workspace/WasmEdge/lib/host/wasi/vinode.cpp" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/vinode.cpp.o" "gcc" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/vinode.cpp.o.d"
  "/workspace/WasmEdge/lib/host/wasi/wasifunc.cpp" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/wasifunc.cpp.o" "gcc" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/wasifunc.cpp.o.d"
  "/workspace/WasmEdge/lib/host/wasi/wasimodule.cpp" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/wasimodule.cpp.o" "gcc" "lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/wasimodule.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/lib/system/CMakeFiles/wasmedgeSystem.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
