# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.23

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /home/linuxbrew/.linuxbrew/Cellar/cmake/3.23.1_1/bin/cmake

# The command to remove a file.
RM = /home/linuxbrew/.linuxbrew/Cellar/cmake/3.23.1_1/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /workspace/WasmEdge

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /workspace/WasmEdge

# Include any dependencies generated for this target.
include lib/api/CMakeFiles/wasmedge_c_shared.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include lib/api/CMakeFiles/wasmedge_c_shared.dir/compiler_depend.make

# Include the progress variables for this target.
include lib/api/CMakeFiles/wasmedge_c_shared.dir/progress.make

# Include the compile flags for this target's objects.
include lib/api/CMakeFiles/wasmedge_c_shared.dir/flags.make

# Object files for target wasmedge_c_shared
wasmedge_c_shared_OBJECTS =

# External object files for target wasmedge_c_shared
wasmedge_c_shared_EXTERNAL_OBJECTS = \
"/workspace/WasmEdge/lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o"

lib/api/libwasmedge_c.so: lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o
lib/api/libwasmedge_c.so: lib/api/CMakeFiles/wasmedge_c_shared.dir/build.make
lib/api/libwasmedge_c.so: lib/vm/libwasmedgeVM.a
lib/api/libwasmedge_c.so: lib/plugin/libwasmedgePlugin.a
lib/api/libwasmedge_c.so: lib/po/libwasmedgePO.a
lib/api/libwasmedge_c.so: lib/loader/libwasmedgeLoader.a
lib/api/libwasmedge_c.so: lib/loader/libwasmedgeLoaderFileMgr.a
lib/api/libwasmedge_c.so: lib/validator/libwasmedgeValidator.a
lib/api/libwasmedge_c.so: lib/executor/libwasmedgeExecutor.a
lib/api/libwasmedge_c.so: lib/host/wasi/libwasmedgeHostModuleWasi.a
lib/api/libwasmedge_c.so: lib/aot/libwasmedgeAOT.a
lib/api/libwasmedge_c.so: lib/system/libwasmedgeSystem.a
lib/api/libwasmedge_c.so: lib/common/libwasmedgeCommon.a
lib/api/libwasmedge_c.so: _deps/spdlog-build/libspdlog.a
lib/api/libwasmedge_c.so: thirdparty/blake3/libutilBlake3.a
lib/api/libwasmedge_c.so: /usr/lib/llvm-12/lib/liblldELF.a
lib/api/libwasmedge_c.so: /usr/lib/llvm-12/lib/liblldDriver.a
lib/api/libwasmedge_c.so: /usr/lib/llvm-12/lib/liblldMachO.a
lib/api/libwasmedge_c.so: /usr/lib/llvm-12/lib/liblldCommon.a
lib/api/libwasmedge_c.so: /usr/lib/llvm-12/lib/liblldReaderWriter.a
lib/api/libwasmedge_c.so: /usr/lib/llvm-12/lib/liblldYAML.a
lib/api/libwasmedge_c.so: /usr/lib/llvm-12/lib/liblldCore.a
lib/api/libwasmedge_c.so: /usr/lib/llvm-12/lib/libLLVM-12.so.1
lib/api/libwasmedge_c.so: lib/api/CMakeFiles/wasmedge_c_shared.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/workspace/WasmEdge/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Linking CXX shared library libwasmedge_c.so"
	cd /workspace/WasmEdge/lib/api && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/wasmedge_c_shared.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
lib/api/CMakeFiles/wasmedge_c_shared.dir/build: lib/api/libwasmedge_c.so
.PHONY : lib/api/CMakeFiles/wasmedge_c_shared.dir/build

lib/api/CMakeFiles/wasmedge_c_shared.dir/clean:
	cd /workspace/WasmEdge/lib/api && $(CMAKE_COMMAND) -P CMakeFiles/wasmedge_c_shared.dir/cmake_clean.cmake
.PHONY : lib/api/CMakeFiles/wasmedge_c_shared.dir/clean

lib/api/CMakeFiles/wasmedge_c_shared.dir/depend:
	cd /workspace/WasmEdge && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /workspace/WasmEdge /workspace/WasmEdge/lib/api /workspace/WasmEdge /workspace/WasmEdge/lib/api /workspace/WasmEdge/lib/api/CMakeFiles/wasmedge_c_shared.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : lib/api/CMakeFiles/wasmedge_c_shared.dir/depend

