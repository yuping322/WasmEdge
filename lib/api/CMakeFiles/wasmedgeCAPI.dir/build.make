# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.23

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /home/linuxbrew/.linuxbrew/Cellar/cmake/3.23.1_1/bin/cmake

# The command to remove a file.
RM = /home/linuxbrew/.linuxbrew/Cellar/cmake/3.23.1_1/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /workspace/WasmEdge

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /workspace/WasmEdge

# Include any dependencies generated for this target.
include lib/api/CMakeFiles/wasmedgeCAPI.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include lib/api/CMakeFiles/wasmedgeCAPI.dir/compiler_depend.make

# Include the progress variables for this target.
include lib/api/CMakeFiles/wasmedgeCAPI.dir/progress.make

# Include the compile flags for this target's objects.
include lib/api/CMakeFiles/wasmedgeCAPI.dir/flags.make

lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o: lib/api/CMakeFiles/wasmedgeCAPI.dir/flags.make
lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o: lib/api/wasmedge.cpp
lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o: lib/api/CMakeFiles/wasmedgeCAPI.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/workspace/WasmEdge/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o"
	cd /workspace/WasmEdge/lib/api && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o -MF CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o.d -o CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o -c /workspace/WasmEdge/lib/api/wasmedge.cpp

lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.i"
	cd /workspace/WasmEdge/lib/api && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /workspace/WasmEdge/lib/api/wasmedge.cpp > CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.i

lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.s"
	cd /workspace/WasmEdge/lib/api && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /workspace/WasmEdge/lib/api/wasmedge.cpp -o CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.s

wasmedgeCAPI: lib/api/CMakeFiles/wasmedgeCAPI.dir/wasmedge.cpp.o
wasmedgeCAPI: lib/api/CMakeFiles/wasmedgeCAPI.dir/build.make
.PHONY : wasmedgeCAPI

# Rule to build all files generated by this target.
lib/api/CMakeFiles/wasmedgeCAPI.dir/build: wasmedgeCAPI
.PHONY : lib/api/CMakeFiles/wasmedgeCAPI.dir/build

lib/api/CMakeFiles/wasmedgeCAPI.dir/clean:
	cd /workspace/WasmEdge/lib/api && $(CMAKE_COMMAND) -P CMakeFiles/wasmedgeCAPI.dir/cmake_clean.cmake
.PHONY : lib/api/CMakeFiles/wasmedgeCAPI.dir/clean

lib/api/CMakeFiles/wasmedgeCAPI.dir/depend:
	cd /workspace/WasmEdge && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /workspace/WasmEdge /workspace/WasmEdge/lib/api /workspace/WasmEdge /workspace/WasmEdge/lib/api /workspace/WasmEdge/lib/api/CMakeFiles/wasmedgeCAPI.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : lib/api/CMakeFiles/wasmedgeCAPI.dir/depend

