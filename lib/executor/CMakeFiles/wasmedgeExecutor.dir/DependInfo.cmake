
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/lib/executor/engine/controlInstr.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/controlInstr.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/controlInstr.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/engine/engine.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/engine.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/engine.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/engine/memoryInstr.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/memoryInstr.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/memoryInstr.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/engine/proxy.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/proxy.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/proxy.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/engine/tableInstr.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/tableInstr.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/tableInstr.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/engine/variableInstr.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/variableInstr.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/engine/variableInstr.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/executor.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/executor.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/executor.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/helper.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/helper.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/helper.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/instantiate/data.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/data.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/data.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/instantiate/elem.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/elem.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/elem.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/instantiate/export.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/export.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/export.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/instantiate/function.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/function.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/function.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/instantiate/global.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/global.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/global.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/instantiate/import.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/import.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/import.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/instantiate/memory.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/memory.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/memory.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/instantiate/module.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/module.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/module.cpp.o.d"
  "/workspace/WasmEdge/lib/executor/instantiate/table.cpp" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/table.cpp.o" "gcc" "lib/executor/CMakeFiles/wasmedgeExecutor.dir/instantiate/table.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/system/CMakeFiles/wasmedgeSystem.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
