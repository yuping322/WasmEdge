
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/lib/system/allocator.cpp" "lib/system/CMakeFiles/wasmedgeSystem.dir/allocator.cpp.o" "gcc" "lib/system/CMakeFiles/wasmedgeSystem.dir/allocator.cpp.o.d"
  "/workspace/WasmEdge/lib/system/fault.cpp" "lib/system/CMakeFiles/wasmedgeSystem.dir/fault.cpp.o" "gcc" "lib/system/CMakeFiles/wasmedgeSystem.dir/fault.cpp.o.d"
  "/workspace/WasmEdge/lib/system/mmap.cpp" "lib/system/CMakeFiles/wasmedgeSystem.dir/mmap.cpp.o" "gcc" "lib/system/CMakeFiles/wasmedgeSystem.dir/mmap.cpp.o.d"
  "/workspace/WasmEdge/lib/system/path.cpp" "lib/system/CMakeFiles/wasmedgeSystem.dir/path.cpp.o" "gcc" "lib/system/CMakeFiles/wasmedgeSystem.dir/path.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
