# CMake generated Testfile for 
# Source directory: /workspace/WasmEdge/lib
# Build directory: /workspace/WasmEdge/lib
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("aot")
subdirs("common")
subdirs("system")
subdirs("plugin")
subdirs("po")
subdirs("loader")
subdirs("validator")
subdirs("executor")
subdirs("host")
subdirs("vm")
subdirs("api")
