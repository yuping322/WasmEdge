
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/lib/vm/vm.cpp" "lib/vm/CMakeFiles/wasmedgeVM.dir/vm.cpp.o" "gcc" "lib/vm/CMakeFiles/wasmedgeVM.dir/vm.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/lib/common/CMakeFiles/wasmedgeCommon.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/system/CMakeFiles/wasmedgeSystem.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/plugin/CMakeFiles/wasmedgePlugin.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/loader/CMakeFiles/wasmedgeLoader.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/validator/CMakeFiles/wasmedgeValidator.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/executor/CMakeFiles/wasmedgeExecutor.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/host/wasi/CMakeFiles/wasmedgeHostModuleWasi.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/po/CMakeFiles/wasmedgePO.dir/DependInfo.cmake"
  "/workspace/WasmEdge/lib/loader/CMakeFiles/wasmedgeLoaderFileMgr.dir/DependInfo.cmake"
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
