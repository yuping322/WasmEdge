
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspace/WasmEdge/lib/common/errinfo.cpp" "lib/common/CMakeFiles/wasmedgeCommon.dir/errinfo.cpp.o" "gcc" "lib/common/CMakeFiles/wasmedgeCommon.dir/errinfo.cpp.o.d"
  "/workspace/WasmEdge/lib/common/hexstr.cpp" "lib/common/CMakeFiles/wasmedgeCommon.dir/hexstr.cpp.o" "gcc" "lib/common/CMakeFiles/wasmedgeCommon.dir/hexstr.cpp.o.d"
  "/workspace/WasmEdge/lib/common/log.cpp" "lib/common/CMakeFiles/wasmedgeCommon.dir/log.cpp.o" "gcc" "lib/common/CMakeFiles/wasmedgeCommon.dir/log.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/workspace/WasmEdge/_deps/spdlog-build/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
